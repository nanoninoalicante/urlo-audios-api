import debug0 from "debug";
const debug = debug0("handler");
// const jwt = require("jsonwebtoken");
// const jwksClient = require("jwks-rsa");
// const client = jwksClient({
//       jwksUri: "https://kueue.eu.auth0.com/.well-known/jwks.json",
//       cache: false,
// });

// const getKey = (header): Promise<string> => {
//       return new Promise((resolve, reject) => {
//             client.getSigningKey(header.kid, (err, key) => {
//                   if (err) {
//                         console.log("error from signing key: ", err);
//                         reject(err);
//                         return;
//                   }
//                   resolve(key.getPublicKey());
//             });
//       });
// };

// const verifyJWT = async (token): Promise<object> => {
//       // eslint-disable-next-line no-useless-catch
//       try {
//             const decoded = jwt.decode(token, { complete: true });
//             const key = await getKey(decoded.header);
//             return await jwt.verify(token, key, {});
//       } catch (e) {
//             throw e;
//       }
// };

const generateSimplePolicy = (isAuthorized = false, context = "") => {
      return {
            isAuthorized,
            context: {
                  stringKey: context,
            },
      };
};

export const handler = async (event: any): Promise<{ isAuthorized: boolean; context: { stringKey: string } }> => {
      debug(JSON.stringify(event));
      try {
            const { headers = {} } = event;
            // const { authorization = null } = headers;
            // if (!authorization) {
            //       throw new Error("Missing auth token");
            // }
            // const token = authorization.slice(7);
            // const verified = await verifyJWT(token).catch(e => {
            //       throw e;
            // });
            const verified = headers;
            console.log("verified: ", verified);

            const policy = generateSimplePolicy(true, verified);
            console.log("policy: ", JSON.stringify(policy));
            return policy;
      } catch (e) {
            console.log("error: ", e.message);
            return generateSimplePolicy(false, e.message);
      }
};
