import AWS from "aws-sdk";
import debug0 from "debug";
import uuid from "uuid";
import { createResponseV3, GlobalResponse } from "../helpers/base";
if (process.env.SERVER === "LOCAL") {
      require("dotenv").config();
}
const debug = debug0("handler");
const parse = require("simple-parse-json");
const s3 = new AWS.S3({
      region: process.env.REGION || "us-east-1",
      accessKeyId: process.env.UPLOADS_AWS_ACCESS_KEY,
      secretAccessKey: process.env.UPLOADS_AWS_SECRET_KEY,
});

export const generateSignedUrl = async (key = undefined): Promise<any> => {
      const params: object = {
            Bucket: "urlo-audio-pending-us-east-1-dev-02-2021",
            Key: key || `${uuid.v4()}.png`,
            Expires: 3600,
      };
      return await s3.getSignedUrlPromise("putObject", params);
};

export const generatePresignedPost = async (
      key = undefined,
      contentType = "image/png"
): Promise<any> => {
      const params: object = {
            Bucket: "urlo-audio-pending-us-east-1-dev-02-2021",
            Fields: {
                  key: key || `${uuid.v4()}.png`,
            },
            Expires: 3600,
            "Content-Type": contentType,
      };
      return new Promise((resolve, reject) => {
            s3.createPresignedPost(params, (err, data) => {
                  if (err) {
                        return reject(err);
                  }
                  return resolve(data);
            });
      });
};

export const handler = async (event: any): Promise<GlobalResponse> => {
      debug(JSON.stringify(event));
      try {
            const body = parse(event.body || {});
            const { key = undefined } = body;
            const { contentType = undefined } = body;
            const signedUrl = await generatePresignedPost(key, contentType);
            debug(signedUrl);
            return createResponseV3({ code: 200, response: signedUrl });
      } catch (e) {
            return createResponseV3({ code: 400, error: e.message });
      }
};
