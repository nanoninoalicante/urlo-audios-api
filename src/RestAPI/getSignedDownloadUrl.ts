import AWS from "aws-sdk";
import debug0 from "debug";
import uuid from "uuid";
import { createResponseV3, GlobalResponse } from "../helpers/base";
require("dotenv").config();
const debug = debug0("handler");
const parse = require("simple-parse-json");
const signer = new AWS.CloudFront.Signer(
      process.env.CF_ACCESS_KEY_ID,
      process.env.CF_PRIVATE_KEY
);

export const handler = async (event: any): Promise<GlobalResponse> => {
      debug(JSON.stringify(event));
      try {
            const body = parse(event.body || {});
            const { key = "" } = body;
            const expires: number = 2 * 24 * 60 * 60 * 1000;
            const signedUrl = await signer.getSignedUrl({
                  url: `https://d37pzmu1fusqos.cloudfront.net/${
                        key || `${uuid.v4()}.png`
                  }`,
                  expires: Math.floor((Date.now() + expires) / 1000), // Unix UTC timestamp for now + 2 days
            });
            debug(signedUrl);
            return createResponseV3({ code: 200, response: signedUrl });
      } catch (e) {
            return createResponseV3({ code: 400, error: e.message });
      }
};
