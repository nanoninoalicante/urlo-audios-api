import test from "ava";
import { generateSignedUrl, generatePresignedPost } from "./getSignedUploadUrl";
import { config } from "dotenv";
config();
import debug from "debug";
const log = debug("*");

test("get signed url", async t => {
      const key = undefined;
      const signedurl = await generateSignedUrl(key).catch(e => {
            console.log("error: ", e.message);
            t.fail(e.message);
      });
      log(signedurl);
      t.pass();
});

test("get presigned post", async t => {
      const key = "test3.m4a";
      const signedurl = await generatePresignedPost(key, "audio/mp4").catch(e => {
            console.log("error: ", e.message);
            t.fail(e.message);
      });
      log(signedurl);
      t.pass();
});