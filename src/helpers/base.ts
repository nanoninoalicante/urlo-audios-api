import { v4 } from "uuid";
import flatten from "flat";
interface Headers {
      "Content-Type": "application/json";
}
export interface GlobalResponse {
      statusCode: number;
      body: string;
      headers: Headers;
}
export interface Response {
      data: object;
      message: string;
      isSuccess: boolean;
      timestamp: Date;
      createdAt: number;
      stage: string;
      version: string;
      env: string;
      region: string;
      functionName: string;
      requestId: string;
}
interface ResponseObject {
      code: number;
      additionalData?: object;
      error?: string;
      response?: object|string;
      requestId?: string;
}

export const createResponseV3 = (data: ResponseObject): GlobalResponse => {
      return {
            statusCode: data.code,
            headers: {
                  "Content-Type": "application/json",
            },
            body: JSON.stringify({
                  ...data.additionalData,
                  data: data.response,
                  message: data.error || "Success",
                  isSuccess: !data.error,
                  timestamp: new Date(),
                  createdAt: new Date().valueOf(),
                  stage: process.env.STAGE || "dev",
                  version: process.env.VERSION || "v2",
                  env: process.env.ENVIRONMENT || "dev",
                  region: process.env.AWS_REGION || "",
                  functionName: process.env.AWS_LAMBDA_FUNCTION_NAME || "",
                  requestId: data.requestId || v4(),
            }),
      };
};

export const remapDataWithNull = (
      keys = {},
      data = {},
      toFlatten = true,
      unFlatten = true
) => {
      let modifiedData = toFlatten ? flatten(data) : data;

      const mappedData = (keys, data) =>
            Object.keys(data).reduce((acc, key) => {
                  const renameObject = () => {
                        if (keys[key] === undefined) {
                              return null;
                        }

                        return {
                              [keys[key]]: data[key] || null,
                        };
                  };

                  return Object.assign(acc, renameObject());
            }, {});

      return unFlatten
            ? flatten.unflatten(mappedData(keys, modifiedData))
            : mappedData(keys, modifiedData);
};
